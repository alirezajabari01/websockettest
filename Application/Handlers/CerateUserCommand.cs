using MediatR;
using Model.Entity;

namespace Application.Handlers;

public class CerateUserCommand:IRequest<int>
{
    public User _user { get; }

    public CerateUserCommand(User user)
    {
        _user = user;
    }
}