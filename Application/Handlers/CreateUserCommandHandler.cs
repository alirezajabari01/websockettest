using Infrastructure.Repository;
using MediatR;
using Model.Entity;

namespace Application.Handlers;

public class CreateUserCommandHandler : IRequestHandler<CerateUserCommand, int>
{
    private IUnitOfWork _unitOfWork { get; }

    public CreateUserCommandHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<int> Handle(CerateUserCommand request, CancellationToken cancellationToken)
    {
        User user = new User()
        {
            Id = request._user.Id,
            CreateDate = DateTime.Now
        };
        _unitOfWork._repository.Create(user);
        return await Task.FromResult(_unitOfWork.SaveChanges());
    }
}