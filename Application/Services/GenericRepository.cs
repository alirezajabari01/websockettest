using Data.DataBase;
using Infrastructure.Repository;
using Microsoft.EntityFrameworkCore;

namespace Application.Services;

public class GenericRepository<T> : IGenericRepository<T> where T :class
{
    private DataBaseContext _context;

    public GenericRepository(DataBaseContext context)
    {
        _context = context; ;
    }
    public void Create(T entity)
    {
        _context.Set<T>().Add(entity);
    }
}