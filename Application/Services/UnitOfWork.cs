using Data.DataBase;
using Infrastructure.Repository;

namespace Application.Services;

public class UnitOfWork:IUnitOfWork
{
    private DataBaseContext _context;
    public IUserRepository _repository { get; }

    public UnitOfWork(DataBaseContext context,IUserRepository _repository)
    {
        _context = context;
        this._repository = _repository;
    }
    public int SaveChanges()
    {
        return _context.SaveChanges();
    }

    public void Dispose()
    {
        _context.Dispose();
    }
}