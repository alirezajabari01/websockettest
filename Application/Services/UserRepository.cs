using Data.DataBase;
using Infrastructure.Repository;
using Model.Entity;

namespace Application.Services;

public class UserRepository :  GenericRepository<User> ,IUserRepository
{
    public UserRepository(DataBaseContext db) : base(db)
    {
        
    }
}