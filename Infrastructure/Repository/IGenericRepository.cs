namespace Infrastructure.Repository;

public interface IGenericRepository<T> where T : class
{
    void Create(T entity);
}