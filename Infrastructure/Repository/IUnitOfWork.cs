namespace Infrastructure.Repository;

public interface IUnitOfWork:IDisposable
{
    IUserRepository _repository { get; }
    int SaveChanges();
}