using Model.Entity;

namespace Infrastructure.Repository;

public interface IUserRepository:IGenericRepository<User>
{
    
}