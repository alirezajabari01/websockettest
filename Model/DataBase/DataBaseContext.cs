using Microsoft.EntityFrameworkCore;
using Model.Entity;

namespace Data.DataBase;

public class DataBaseContext:DbContext
{
    public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
    {
        
    }

    public DbSet<User> Users { get; set; }
}