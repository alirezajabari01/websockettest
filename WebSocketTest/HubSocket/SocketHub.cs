using Microsoft.AspNetCore.SignalR;

namespace WebSocketTest.HubSocket
{
    public class SocketHub : Hub
    {
        private IHubContext<SocketHub> _context;

        public SocketHub(IHubContext<SocketHub> context)
        {
            _context = context;
        }
        
        public async Task Start()
        {
        }
    }
}
