using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using WebSocketTest.Data;
using WebSocketTest.HubSocket;
using WebSocketTest.Repository;
using WebSocketTest.WebSocket;

var builder = WebApplication.CreateBuilder(args);
const string allowSpecificOrigins = "_allowSpecificOrigins";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: allowSpecificOrigins,
        corsPolicyBuilder =>
        {
            corsPolicyBuilder.WithOrigins("http://localhost:3000", "https://dev.octalbit.com").AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials().Build();
        });
});
// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddSignalR(opt =>
{
    
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<DatabaseContext>
    (options =>
    options.UseSqlServer("Password=ABC1%@Jbry5;Persist Security Info=True;User ID=sa;Initial Catalog=SocketTest;Data Source=DESKTOP-LJMRBHR\\ENTERPRISE2019")
    );

builder.Services.AddScoped<DatabaseContext>();
builder.Services.AddScoped<IBaseRepo, BaseRepo>();
builder.Services.AddSingleton<TestSocketHub>();
var app = builder.Build();

var lifetime = app.Services.GetRequiredService<IHostApplicationLifetime>();
var registerer = app.Services.GetRequiredService<TestSocketHub>();
lifetime.ApplicationStarted.Register(() => { Task.Run(async () => { await registerer.Start(); }); });
lifetime.ApplicationStopping.Register(() => registerer.Dispose());
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseAuthorization();
app.UseCors(allowSpecificOrigins);
app.MapControllers();
app.MapHub<TestSocketHub>("/hub");

app.Run();
