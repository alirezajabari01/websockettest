﻿using WebSocketTest.Data;

namespace WebSocketTest.Repository
{
    public class BaseRepo : IBaseRepo
    {
        public DatabaseContext _db;

        public BaseRepo(DatabaseContext db)
        {
            _db = db;
        }

        public string CreateUser(User user)
        {
             _db.Users.Add(user);
             _db.SaveChanges();
             return "done ";
        }

        public List<User> Get()
        {
            return _db.Users.ToList();
        }
    }
}
