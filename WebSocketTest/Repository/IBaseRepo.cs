﻿using WebSocketTest.Data;

namespace WebSocketTest.Repository
{
    public interface IBaseRepo
    {
        string CreateUser(User user);
        List<User> Get();
    }
}
