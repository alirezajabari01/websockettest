﻿using Microsoft.AspNetCore.SignalR;
using System.Net.WebSockets;
using System.Text;
using WebSocketTest.Data;
using WebSocketTest.HubSocket;
using WebSocketTest.Repository;

namespace WebSocketTest.WebSocket
{
    public class Registerer
    {
        private IHubContext<SocketHub> hubContext;
        private IBaseRepo baseRepo;

        public Registerer(IHubContext<SocketHub> hubContext, IBaseRepo baseRepo)
        {
            this.hubContext = hubContext;
            this.baseRepo = baseRepo;
        }

        public async Task Start()
        {
            await Register();
        }

        public async Task Dispose()
        {

        }

        private async Task Register()
        {
            using (var ws = new ClientWebSocket())
            {

                await ws.ConnectAsync(
                                   new Uri(
                                       "wss://demo.piesocket.com/v3/channel_1?api_key=VCXCEuvhGcBDP7XhiJJUDvR1e1D3eiVjgZ9VRiaV&notify_self"
                                       ), CancellationToken.None
                                   );

                byte[] buffer = new byte[256];

                var result = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    await ws.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                }
                else
                {

                }
                HandleMessage(buffer, result.Count);
                // send message 
                CancellationToken cancellation = new CancellationToken();

                //

                // decode recieved message 
                var token = CancellationToken.None;
                var bufferss = new ArraySegment<Byte>(new Byte[4096]);
                var received = await ws.ReceiveAsync(bufferss, token);
                var recievedMessage = "null";
                switch (received.MessageType)
                {
                    case WebSocketMessageType.Text:
                        recievedMessage = Encoding.UTF8.GetString(bufferss.Array,
                            bufferss.Offset,
                            bufferss.Count);
                        break;
                }
                //
                var User = new User()
                {
                    Text = recievedMessage
                };


                string res = baseRepo.CreateUser(User);
                var encodedResponse = Encoding.UTF8.GetBytes(res);
                var buffers = new ArraySegment<Byte>(encodedResponse, 0, encodedResponse.Length);
                await ws.SendAsync(buffers, WebSocketMessageType.Text, true, cancellation);

            }
        }
        private static void HandleMessage(byte[] buffer, int count)
        {
            Console.WriteLine($"Received {BitConverter.ToString(buffer, 0, count)}");
        }
    }
}
