using System.Net.WebSockets;
using System.Text;
using Microsoft.AspNetCore.SignalR;
using WebSocketTest.Data;
using WebSocketTest.Repository;

namespace WebSocketTest.WebSocket;

public class TestSocketHub :Hub
{
    private IBaseRepo baseRepo;

    public TestSocketHub(IBaseRepo baseRepo)
    {
        this.baseRepo = baseRepo;
    }

    public async Task Start()
    {
        await Register();
    }

    public async Task Dispose()
    {
        
    }

    public async Task Register()
    {
         using (var ws = new ClientWebSocket())
            {

                await ws.ConnectAsync(
                                   new Uri(
                                       "wss://demo.piesocket.com/v3/channel_1?api_key=VCXCEuvhGcBDP7XhiJJUDvR1e1D3eiVjgZ9VRiaV&notify_self"
                                       ), CancellationToken.None
                                   );

                byte[] buffer = new byte[256];

                var result = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    await ws.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
                }
                else
                {
                    CancellationToken cancellation = new CancellationToken();

                    //

                    // decode recieved message 
                    var token = CancellationToken.None;
                    var bufferss = new ArraySegment<Byte>(new Byte[4096]);
                    var received = await ws.ReceiveAsync(bufferss, token);
                    var recievedMessage = "null";
                    switch (received.MessageType)
                    {
                        case WebSocketMessageType.Text:
                            recievedMessage = Encoding.UTF8.GetString(bufferss.Array,
                                bufferss.Offset,
                                bufferss.Count);
                            break;
                    }
                    //
                    var user = new User()
                    {
                        Text = "hello guys"
                    };


                    string res =baseRepo.CreateUser(user);
                    var encodedResponse = Encoding.UTF8.GetBytes("rf");
                    var buffers = new ArraySegment<Byte>(encodedResponse, 0, encodedResponse.Length);
                    await ws.SendAsync(buffers, WebSocketMessageType.Text, true, cancellation);

                }
                //HandleMessage(buffer, result.Count);
                // send message 
               
            }
    }
}